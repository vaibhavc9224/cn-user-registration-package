import { NgModule } from "@angular/core";
import { CNUserRegistration } from "./cn-user-registration";
export function CNUserRegistrationFactory() {
    return new CNUserRegistration();
}
export var CNUserRegistrationModule = (function () {
    function CNUserRegistrationModule() {
    }
    CNUserRegistrationModule.decorators = [
        { type: NgModule, args: [{
                    providers: [
                        {
                            provide: CNUserRegistration,
                            useFactory: CNUserRegistrationFactory
                        }
                    ]
                },] },
    ];
    /** @nocollapse */
    CNUserRegistrationModule.ctorParameters = function () { return []; };
    return CNUserRegistrationModule;
}());
//# sourceMappingURL=cn-user-registration.module.js.map