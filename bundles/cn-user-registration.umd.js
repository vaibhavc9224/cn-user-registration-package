(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core')) :
    typeof define === 'function' && define.amd ? define(['exports', '@angular/core'], factory) :
    (factory((global['cn-user-registration'] = global['cn-user-registration'] || {}),global.ng.core));
}(this, (function (exports,_angular_core) { 'use strict';

var CNUserRegistration = (function () {
    function CNUserRegistration() {
    }
    CNUserRegistration.prototype.get = function () {
        return "Hello World";
    };
    CNUserRegistration.decorators = [
        { type: _angular_core.Injectable },
    ];
    /** @nocollapse */
    CNUserRegistration.ctorParameters = function () { return []; };
    return CNUserRegistration;
}());

function CNUserRegistrationFactory() {
    return new CNUserRegistration();
}
var CNUserRegistrationModule = (function () {
    function CNUserRegistrationModule() {
    }
    CNUserRegistrationModule.decorators = [
        { type: _angular_core.NgModule, args: [{
                    providers: [
                        {
                            provide: CNUserRegistration,
                            useFactory: CNUserRegistrationFactory
                        }
                    ]
                },] },
    ];
    /** @nocollapse */
    CNUserRegistrationModule.ctorParameters = function () { return []; };
    return CNUserRegistrationModule;
}());

exports.CNUserRegistrationModule = CNUserRegistrationModule;
exports.CNUserRegistration = CNUserRegistration;

Object.defineProperty(exports, '__esModule', { value: true });

})));
